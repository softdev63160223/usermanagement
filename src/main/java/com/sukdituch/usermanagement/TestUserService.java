/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sukdituch.usermanagement;

/**
 *
 * @author focus
 */
public class TestUserService {

    public static void main(String[] args) {
        UserService.addUser("user2", "password");
        System.out.println(UserService.getUsers());
        UserService.addUser("Herry", "Maguire");
        System.out.println(UserService.getUsers());
        
        // Update
        User user = UserService.getUser(3);
        System.out.println(user);
        user.setUserPass("1234");
        UserService.updateUser(3, user);
        System.out.println(UserService.getUser(3));
        
        //Delete
        UserService.delUser(3);
        System.out.println(UserService.getUsers());
        
        //Login
        System.out.println(UserService.login("admin", "password"));
        
    }
}
