/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sukdituch.usermanagement;

import java.util.ArrayList;

/**
 *
 * @author focus
 */
public class UserService {

    private static ArrayList<User> userList = new ArrayList<>(); //Static เกิดเลยโดยไม่ต้องสร้าง Object และมีแค่อันเดียว
    //Mockup ( จำลองข้อมูลขึนมา )

    static { //static {} คือ static block จะถูก load เมื่อมีการ load หรือ เมื่อมีการ comply จะทำด้วยเลย
        userList.add(new User("admin", "password"));
        userList.add(new User("user1", "password"));
    }

    // Create
    public static boolean addUser(User user) {
        userList.add(user);
        save();
        return true;
    }

    // Overload function แต่ไม่ได้ใช้
    public static boolean addUser(String userName, String userPass) {
        userList.add(new User(userName, userPass));
        return true;
    }

    // Update
    public static boolean updateUser(int index, User user) {
        userList.set(index, user);
        save();
        return true;
    }

    // Read 1 user
    public static User getUser(int index) {
        if(index > userList.size()-1){
            return null;
        }
        return userList.get(index);
    }

    // Read all user
    public static ArrayList<User> getUsers() {
        return userList;
    }

    // Search userName
    public static ArrayList<User> searchUserName(String searchText) {
        ArrayList<User> list = new ArrayList<>();
        for (User user : userList) {
            if (user.getUserName().startsWith(searchText)) {
                list.add(user);
            }
        }
        return list;
    }

    // Delete user index
    public static boolean delUser(int index) {
        userList.remove(index);
        save();
        return true;
    }

    // Delete user object
    public static boolean delUser(User user) {
        userList.remove(user);
        save();
        return true;
    }
    
    // Login 
    public static User login(String userName, String userPass){
        for(User user: userList){
            if(user.getUserName().equals(userName)  && user.getUserPass().equals(userPass)){
                return user;
            }
        }
        return null; // return null คือ login ไม่ถูก หรือ ไม่ได้
    }

    public static void save(){
        
    }
    
    public static void load(){
        
    }
    
}
